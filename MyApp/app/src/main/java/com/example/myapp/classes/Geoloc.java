package com.example.myapp.classes;

import com.google.gson.annotations.SerializedName;

public class Geoloc {
    public float lat;
    @SerializedName("long")
    public float lon;
}
