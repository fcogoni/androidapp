package com.example.myapp;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.*;
import com.example.myapp.classes.FileResult;
import com.example.rest.ApiHelper;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {

    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_detail);
        intent = getIntent();
        if (intent != null) {
            String title = intent.getStringExtra("title");
            ((TextView)findViewById(R.id.detailTextViewTitle)).setText(title);
        }

    }
}
