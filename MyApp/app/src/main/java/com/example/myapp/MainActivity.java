package com.example.myapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.*;
import com.example.myapp.classes.FileResult;
import com.example.rest.ApiHelper;
import android.view.View;
import android.widget.*;
import android.content.Intent;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ApiHelper.getInstance().getFileResultApi().getFileResult().enqueue(new Callback<FileResult>() {
            @Override
            public void onResponse(Call<FileResult> call, Response<FileResult> response) {
                if (response.isSuccessful()) {
                    mRecyclerView = findViewById(R.id.mainRecyclerView);


                    mLayoutManager = new LinearLayoutManager(MainActivity.this);
                    mRecyclerView.setLayoutManager(mLayoutManager);

                    // specify an adapter (see also next example)
                    mAdapter = new MyAdapter(response.body(),MainActivity.this);
                    mRecyclerView.setAdapter(mAdapter);

                }
            }

            @Override
            public void onFailure(Call<FileResult> call, Throwable t) {
                //TODO handle failure :)
            }
        });


    }

    public void listener() {
        Intent intent = new Intent(this, DetailActivity.class);
        MyAdapter myAdapter = ((MyAdapter)mRecyclerView.getAdapter());
        //ArrayList<String> liste = new ArrayList<String>();
        //liste.add();
        intent.putExtra("Title", myAdapter.mDataset.movieShowtimes[myAdapter.m_positionActuelle].onShow.movie.title);
        //((TextView) findViewById(R.id.detailTextViewTitle)).setText(myAdapter.mDataset.movieShowtimes[myAdapter.m_positionActuelle].onShow.movie.title);


        startActivity(intent);
    }

}
