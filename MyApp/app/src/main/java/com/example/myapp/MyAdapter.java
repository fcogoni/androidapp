package com.example.myapp;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.*;
import com.example.myapp.classes.FileResult;
import com.example.rest.ApiHelper;
import android.view.LayoutInflater;
import android.widget.TextView;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    public FileResult mDataset;
    private MainActivity mainActivity;
    public int m_positionActuelle = 0;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public FrameLayout mFrameLayout;
        public MyViewHolder(FrameLayout v) {
            super(v);
            mFrameLayout = v;
        }
    }

    public MyAdapter(FileResult myDataset, MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        mDataset = myDataset;
    }

    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        FrameLayout v = (FrameLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_cell, parent, false);


        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        TextView mTextView = ((TextView)holder.mFrameLayout.findViewById(R.id.textViewReleaseTitle));
        mTextView.setText(mDataset.movieShowtimes[position].onShow.movie.title);
        TextView.OnClickListener clickListener = new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_positionActuelle = position;
                mainActivity.listener();
            }
        };
        mTextView.setOnClickListener(clickListener);
    }


    @Override
    public int getItemCount() {
        return mDataset.movieShowtimes.length;
    }


}