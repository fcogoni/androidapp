package com.example.myapp.classes;

public class MovieShowtimes {
    public boolean preview;
    public boolean releaseWeek;
    public OnShow onShow;
    public Version version;
    public ScreenFormat screenFormat;
    public String display;
    public Scr scr[];

    public class OnShow{
        public Movie movie;

        public class Movie{
            public int code;
            public String title;
            public CastingShots castingShorts;
            public Release release;
            public int runtime;
            public Genre genre[];
            public Poster poster;
            public Trailer trailer;
            public String trailerEmbed;
            public Link link[];
            public Statistics statistics;

            public class CastingShots {
                public String directors;
                public String actors;
            }
            public class Release  {
                public String releaseDate;
            }

            public class Genre {
                public int code;
                public String Name;
            }

            public class Poster {
                public String path;
                public String href;
            }

            public class Trailer {
                public int code;
                public String href;
            }

            public class Link {
                public String rel;
                public String name;
                public String href;
            }

            public class Statistics {
                public float pressRating;
                public float pressReviewCount;
                public float userRating;
                public float userReviewCount;
                public float userRatingCount;
                public float editorialRatingCount;
            }
        }
    }

    public class Version {
        public Boolean original;
        public int code;
        public int lang;
        public String name;
    }

    public class ScreenFormat {
        public int code;
        public String name;
    }

    public class Scr {
        public String d;
        public T t[];

        public class T {
            public String code;
            public float p;
            public String tk;
        }
    }
}
