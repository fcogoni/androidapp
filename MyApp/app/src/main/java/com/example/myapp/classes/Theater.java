package com.example.myapp.classes;

public class Theater {
    public String code;
    public String name;
    public String address;
    public String postalCode;
    public String city;
    public String area;
    public Picture picture;
    public CinemaChain cinemaChain;
    public int hasPRMAccess;
    public int openToSales;
    public Geoloc geoloc;
    public Link link[];

}
