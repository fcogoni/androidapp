package com.example.rest;

import com.example.myapp.classes.FileResult;

import retrofit2.Call;
import retrofit2.http.GET;
//import retrofit2.http.Path;

public interface FileResultApi {

    /**
     * Call a webservice with dynamic path
     * Exemple :
     * "https://api.myjson.com/bins/31245"
     * base url : https://api.myjson.com/
     * path : bins/{id}
     * where id = 31245
     * So the method will be called using getNews("31235");
     */

    //@GET("bins/{id}")
    //Call<FileResult> getFileResult(@Path("id") String id);

    /**
     * Call a webservice with a static path
     */
    @GET("https://etudiants.openium.fr/pam/cine.json")
    Call<FileResult> getFileResult();
}
