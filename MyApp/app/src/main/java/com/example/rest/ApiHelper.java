package com.example.rest;

import com.example.myapp.classes.FileResult;
import retrofit2.converter.gson.GsonConverterFactory;

import retrofit2.Retrofit;

public class ApiHelper {
    private FileResultApi fileResultApi;

    public FileResultApi getFileResultApi() {
        return fileResultApi;
    }

    private ApiHelper() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://api.myjson.com/").addConverterFactory(GsonConverterFactory.create()).build();
        fileResultApi = retrofit.create(FileResultApi.class);
    }

    private static volatile ApiHelper instance;


    public static synchronized ApiHelper getInstance() {
        if (instance == null) {
            instance = new ApiHelper();
        }
        return instance;
    }
}